import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.io.File;
import java.time.LocalDate;


class Main{
    static int checkTicketFile(){
        int i = 1;
        for(;;){
            File ticket = new File("Ticket"+String.valueOf(i)+".txt");
            boolean isExists = ticket.exists();

            if(isExists == false){
                return i;
            }
            i += 1;
        }
    }
    
    public static void printTicket(int movie, int movieTime, String seat){
        LocalDate date = LocalDate.now();
        String getMovie ="";
        String getMovieTime ="";
        String studio ="";

        switch(movie){
            case 1: 
                getMovie = "Mortal Kombat";
                studio = "1";
                break;
            case 2:
                studio = "2";
                getMovie = "King Kong";
                break;

        }
        switch(movieTime){
            case 1:
                getMovieTime = "17.00";
                break;
            case 2:
                getMovieTime ="19.00";
                break;
            case 3:
                getMovieTime ="21.00";
                break;
        }

        try{
            int ticketNumber = checkTicketFile();
            FileWriter ticketFile = new FileWriter("Ticket"+String.valueOf(ticketNumber)+".txt");
            
            ticketFile.write("Tanggal : " + date +"\n" +
                            "Judul Film : " + getMovie + "\n"+
                            "Jam Tayang : " + getMovieTime + "\n"+
                            "Posisi Duduk : "+seat + "\n" +
                            "Studio : "+studio
            );
            ticketFile.close();
            System.out.println("Tiket Berhasil Dibuat!");

        }
        catch(IOException e){

        }


    }
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);
        
        // Memilih Film
        System.out.println("Pilih Film : \n" + "1. Mortal Combat\n" + "2. King Kong");
        int getMovie = userInput.nextInt();
        
        // Memilih Jam Tayang
        System.out.println("Pilih jam tayang : \n" + "(1) 17.00\n" + "(2) 19.00 \n" + "(3) 21.00");
        int getMovieTime = userInput.nextInt();

        // Memilih Tempat Duduk
        System.out.println("Pilih tempat duduk : ");
        String getSeat = userInput.next();

        // Mencetak ticket
        printTicket(getMovie, getMovieTime, getSeat);

    }
}